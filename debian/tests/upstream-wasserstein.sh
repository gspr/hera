#!/bin/bash

set -e
set -u

tmpdir=$AUTOPKGTEST_TMP/wasserstein

mkdir -p $tmpdir
cp -R $PWD/wasserstein/tests/* $tmpdir

cd $tmpdir
ln -s /usr/include/catch2 catch

set -x
g++ -O2 -I/usr/include/hera/wasserstein -o test tests_main.cpp test_hera_wasserstein.cpp test_hera_wasserstein_pure_geom.cpp
cd data
../test
