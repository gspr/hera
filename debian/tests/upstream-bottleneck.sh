#!/bin/bash

set -e
set -u

tmpdir=$AUTOPKGTEST_TMP/bottleneck

mkdir -p $tmpdir
cp -R $PWD/bottleneck/tests/* $tmpdir

cd $tmpdir
ln -s /usr/include/catch2 catch

set -x
g++ -O2 -I/usr/include/hera/bottleneck -o test tests_main.cpp test_hera_bottleneck.cpp
./test
cd data
../test

